let swApi;
(
    function(){
        "use strict";
        swApi = {
            getCharacter: ()=>{
                    return fetch('https://swapi.co/api/people/')
                    .then(chars => chars.json())
                    .then(chars =>{
                        const random = Math.floor(Math.random()*chars.count)
                        return fetch('https://swapi.co/api/people/' + random)
                            .then(char => char.json())
                    })
            },
            getSpecie: (urlSpecie)=>{
                if(!urlSpecie)
                    throw new Error('no url specie provided')

                return fetch(urlSpecie)
                    .then(specie => specie.json())
            }
        }
    }
)()