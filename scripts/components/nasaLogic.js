let getPictureFromNasa
(function(){
    "use strict";
    
    const $astroTogle = $('.toggle')
    const $astro__info = $('.astro__info')
    
    getPictureFromNasa = function(){
        try{
            nasaApi.getPicture()
            .then(res=>{
                showImage(res.url)
                showTitle(res.title)
                showExplanation(res.explanation)
            })
        }catch(err){
            console.error(err.message)
        }
    }
    
    function showImage(url){
        $('.astro__img').append( `<img src=${url} height="100%" width="100%">`)
    }

    function showTitle(title){
        $('.astro__info h2').text(title)
    }

    function showExplanation(explanation){
        $('.astro__info p').text(explanation)
    }

    $astroTogle.on('click',()=>{
        if($astro__info.css('display')==='none'){
            showInfo()
        }else{
            hideInfo()
        }  
    })
    function showInfo(){
        $astro__info.css('display', 'block')
        $astroTogle.css('background-color', 'red')
        $astroTogle.text('x')
    }
    function hideInfo(){
        $astro__info.css('display', 'none')
        $astroTogle.css('background-color', '#2C3DC6')
        $astroTogle.text('i')
    }
})()