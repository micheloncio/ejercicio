let getCharacterFromSW
(function(){
    "use strict";
    getCharacterFromSW = function(){
        try{
            swApi.getCharacter()
            .then(char=>{
                if(Number(char.height) <= 170){
                    return getCharacterFromSW()
                }else{
                    showCharacter(char)
                    showSpecie(char.species[0])
                }
            })
        }catch(err){
            console.error(err.message)
        }
    }

    function showCharacter(char){
        $('.sw__character').append( `<h1>Nombre: ${char.name}</h1>`)
        $('.sw__character').append( `<h1>Peso: ${char.mass}</h1>`)
        $('.sw__character').append( `<h1>Estatura: ${char.height}</h1>`)
    }
    
    function showSpecie(urlSpecie){
        try{
            swApi.getSpecie(urlSpecie)
            .then(specie=>{
                $('.sw__character').append( `<h1>Especie: ${specie.name}</h1>`)
            })
        }catch(err){
            console.error(err.message)
        }
    }
})()
